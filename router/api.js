const express = require('express');
const passport = require('passport');
const config = require('../config/database');
const User = require('../models/user');
require('../config/passport')(passport);

const multer = require('multer');
// const upload=multer({dest:'uploads/'});
const crypto = require('crypto');

const storage=multer.diskStorage({
    destination:function(req,file,cb){
       // console.log(file);
        cb(null,'./uploads/');
    },
     filename:function(req,file,cb){
        crypto.pseudoRandomBytes(16, function(err, raw) {
            cb(null, ''+raw.toString('hex')+file.originalname);
            });
            // crypto.randomBytes(64).toString('hex');
     }
});



const fileFilter=(req,file,cb)=>{
    if(file.mimetype==='image/jpeg' || file.mimetype==='image/png' || file.mimetype==='image/gif'){
        cb(null,true);   //new Error(msg{})  function for error handling
    }else{
        cb(null,false);
    }
};

const upload=multer({
    storage:storage,
    limits:{fileSize:1024*1024*5},
    fileFilter:fileFilter
});

const storageAudio=multer.diskStorage({
    destination:function(req,file,cb){
       // console.log(file);
        cb(null,'./uploads/audios');
    },
     filename:function(req,file,cb){
        cb(null, ''+file.originalname);
     }
});
const audioFileFilter=(req,file,cb)=>{
    if(file.mimetype==='audio/mp3'){
        cb(null,true);   //new Error(msg{})  function for error handling
    }else{
        cb(null,false);
    }
};
const uploadAudio=multer({
    storage:storageAudio,
   // limits:{fileSize:1*1000*1000},
    fileFilter:audioFileFilter
});

var userController = require('../controllers/UserController');
var radioController = require('../controllers/RadioController');
var radioOrgController = require('../controllers/RadioOrganizationController');
var channelController = require('../controllers/ChannelController');
var rjprofileController = require('../controllers/RJProfileController');
var scheduleController = require('../controllers/ScheduleController');
var programController = require('../controllers/ProgramController');
var episodeController = require('../controllers/EpisodeController');
var clientController = require('../controllers/ClientController');
var podcastController= require('../controllers/PodcastController');

//this is core function
module.exports=function(app){
    
 
const apiRoutes=express.Router();
const userRoutes=express.Router();
const radioRoutes=express.Router();
const radioOrgRoutes=express.Router();
const channelRoutes=express.Router();
const programRoutes=express.Router();
const rjprofileRoutes=express.Router();
const scheduleRoutes=express.Router();
const episodeRoutes=express.Router();
const clientRoutes=express.Router();
const podcastRoutes=express.Router();


// // Profile
// router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => { 
//     res.json({user: req.user});
    
//   });

//adminUser Routes++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
apiRoutes.use('/users',userRoutes);


// Register
userRoutes.post('/register', userController.registerUser);

// Authenticate
userRoutes.post('/authenticate',userController.authenticateUser);

//Profile
userRoutes.get('/profile', passport.authenticate('jwt', {session:false}),userController.getProfile);



// radio routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append postRoutes to apiRoutes
apiRoutes.use('/radios',radioRoutes);

//get all radios
 radioRoutes.get('/',radioController.getAllRadios);
// //get post by id
// radioRoutes.get('/:id',radioController.getRadioById);
//create post
//  radioRoutes.post('/',radioController.createRadio);
//create radios+also can create new pradesh
radioRoutes.put('/:pradeshName',radioController.updateRadio);
//delete radios
 radioRoutes.delete('/:pradeshName/:linkid',radioController.deleteRadio);
  //update radios array
 radioRoutes.put('/:pradeshName/:linkid',radioController.editRadio);

 // radioOrg routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append postRoutes to apiRoutes
apiRoutes.use('/radioOrganizations',radioOrgRoutes);

radioOrgRoutes.get('/',radioOrgController.getAllRadioOrganizations);
radioOrgRoutes.get('/:id',radioOrgController.getRadioOrganizationById);
// radioOrgRoutes.post('/',upload.single('radioImage'),radioOrgController.createRadioOrganization);
radioOrgRoutes.put('/:id',radioOrgController.updateRadioOrganization);
radioOrgRoutes.delete('/:id',radioOrgController.deleteRadioOrganization);
radioOrgRoutes.get('/:radioOrganizationId/channels',radioOrgController.getChannelsOfRadioOrganization);

radioOrgRoutes.post('/',upload.single('radioOrganizationImage'),radioOrgController.createRadioOrganization);
// radioOrgRoutes.post('/image',upload.single('radioOrganizationImage'),radioOrgController.organizationImage);
// channel routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append channelRoutes to apiRoutes
apiRoutes.use('/channels',channelRoutes);

channelRoutes.get('/',channelController.getAllChannels);
channelRoutes.get('/:id',channelController.getChannelById);
channelRoutes.get('/organization/:organizationId',channelController.getChannelByOrganizationId);
channelRoutes.post('/:id',upload.single('radioOrganizationImage'),channelController.createChannel);
channelRoutes.put('/:id',channelController.updateChannel);
channelRoutes.delete('/:id',channelController.deleteChannel);
channelRoutes.get('/pradesh/:regionName',channelController.getAllChannelsByPradesh);

// channel routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append channelRoutes to apiRoutes
apiRoutes.use('/programs',programRoutes);

programRoutes.get('/',programController.getAllPrograms);
programRoutes.get('/:id',programController.getProgramById);
programRoutes.get('/channel/:channelId',programController.getProgramsByChannelId);
programRoutes.post('/channel/:channelId',programController.createProgram);
programRoutes.put('/:id',programController.updateProgram);
programRoutes.delete('/:id',programController.deleteProgram);

// channel routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append channelRoutes to apiRoutes
apiRoutes.use('/rjprofiles',rjprofileRoutes);

rjprofileRoutes.get('/',rjprofileController.getAllRJProfiles);
rjprofileRoutes.get('/:id',rjprofileController.getRJProfileById);  
rjprofileRoutes.get('/channel/:channelId',rjprofileController.getRJProfileOfChannel);
 rjprofileRoutes.post('/channel/:channelId',rjprofileController.createRJProfile);
 rjprofileRoutes.put('/:id',rjprofileController.updateRJProfile);
 rjprofileRoutes.delete('/:id',rjprofileController.deleteRJProfile);

 // channel routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append channelRoutes to apiRoutes
apiRoutes.use('/schedules',scheduleRoutes);

scheduleRoutes.get('/',scheduleController.getAllSchedules);
scheduleRoutes.get('/:id',scheduleController.getScheduleById);  
scheduleRoutes.get('/channel/:channelId',scheduleController.getScheduleOfChannel);
scheduleRoutes.get('/channel/:channelId/day/:dayName',scheduleController.getSchedulesByDay);
scheduleRoutes.post('/channel/:channelId',scheduleController.createSchedule);
scheduleRoutes.put('/:id',scheduleController.updateSchedule);
scheduleRoutes.delete('/:id',scheduleController.deleteSchedule);

// episodes routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append episodeRoutes to apiRoutes
apiRoutes.use('/episodes',episodeRoutes);

episodeRoutes.get('/',episodeController.getAllEpisodes);
 episodeRoutes.get('/:id',episodeController.getEpisodeById);
 episodeRoutes.post('/:programId',episodeController.createEpisode);
 episodeRoutes.put('/:id',episodeController.updateEpisode);
 episodeRoutes.delete('/:id',episodeController.deleteEpisode);

 // clients routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append clientRoutes to apiRoutes
apiRoutes.use('/clients',clientRoutes);

clientRoutes.get('/',clientController.getAllClients);
//  episodeRoutes.get('/:id',episodeController.getEpisodeById);
clientRoutes.post('/',clientController.createClient);
//  episodeRoutes.put('/:id',episodeController.updateEpisode);
//  episodeRoutes.delete('/:id',episodeController.deleteEpisode);

// podcast routes+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//append podcastRoutes to apiRoutes
apiRoutes.use('/podcasts',podcastRoutes);

podcastRoutes.get('/',podcastController.getAllPodcasts);
podcastRoutes.get('/:id',podcastController.getPodcastById);
 podcastRoutes.get('/channel/:parentChannelID',podcastController.getPodcastsOfChannel);
podcastRoutes.post('/',podcastController.createPodcast);
podcastRoutes.put('/:id',podcastController.updatePodcast);
podcastRoutes.post('/:id/createEpisode',uploadAudio.single('audioFile'),podcastController.updateEpisodesOfPodcast);
podcastRoutes.delete('/:id',podcastController.deletePodcast);



// *************************************************************************

//append routes++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
app.use('/api',apiRoutes);

}  //closing core function
