const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config/database');
const morgan = require('morgan');
var passport  = require('passport');

 var router=require('./router/api');

// Connect To Database
mongoose.Promise = global.Promise;
mongoose.connect(config.database, { useMongoClient: true });

// On Connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database '+config.database);
  });

  // On Error
mongoose.connection.on('error', (err) => {
    console.log('Database error: '+err);
  });



  // Port Number
const port = 3000;

app.use(morgan('dev'));

app.use('/uploads',express.static('uploads'));
// Body Parser Middleware
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//Add headers
app.use(function (req, res, next) {
  
      // Website you wish to allow to connect
      res.setHeader('Access-Control-Allow-Origin', '*');
  
      // Request methods you wish to allow
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
      // Request headers you wish to allow
      res.setHeader('Access-Control-Allow-Headers','No-Auth,X-Requested-With,content-type,Authorization' ); 
  
      // Set to true if you need the website to include cookies in the requests sent
      // to the API (e.g. in case you use sessions)
      res.setHeader('Access-Control-Allow-Credentials', true);
  
      // Pass to next layer of middleware
      next();
  });

router(app);






app.get('/', (req, res) => {
    res.send('PLEASE USE /api/sliders');
  });


  

    
  // Start Server
  app.listen(port,"0.0.0.0", function(){
    console.log('Server started on port '+port);
  });
  