var mongoose=require('mongoose');

var channelSchema=mongoose.Schema({
    channelName:{type:String,required:true},  
    channelLogo:String,
    url:String,
    port:String,
    region:[String],
    schedule:[],//{type:mongoose.Schema.Types.ObjectId,ref:'Schedule'}
    rjProfile:[],
    parentOrganization:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Radioorganization'
    }
});
 
var Channel=module.exports=mongoose.model('Channel', channelSchema);
