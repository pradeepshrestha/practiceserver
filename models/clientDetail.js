const mongoose = require('mongoose');


// User Schema
const clientDetailSchema = mongoose.Schema({
    parentClient:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Client'
    },
  organizationName:{
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
 organizationEmail: {
    type: String,
    required: true
  },
  facebookId:String,
  registered_date:String,
  created_at:String,
  updated_at:String,
  user_type:{
      type:String,
      default:"admin"
    },
    status: {
        type: String,
        enum : ['0','1'],
        default: '0'
    },
});

const ClientDetail = module.exports = mongoose.model('ClientDetail', clientDetailSchema);


