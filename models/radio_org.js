
var mongoose=require('mongoose');

var radio_orgSchema=mongoose.Schema({
  organizationName:{type:String,required:true},  
  orgLogo:{type:String},
  radioOrganizationImage:String,
  radioChannels:[
    {type:mongoose.Schema.Types.ObjectId,ref:'Channel'}
  ]

      
  
});
 
var RadioOrganization=module.exports=mongoose.model('Radioorganization', radio_orgSchema);
