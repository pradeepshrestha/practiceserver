var mongoose=require('mongoose');

var episodeSchema=mongoose.Schema({
    parentProgram:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Program'
    },
    episodeName:String,
    episodeNumber:String  
    
});
 
var Episode=module.exports=mongoose.model('Episode', episodeSchema);
