var mongoose=require('mongoose');

var scheduleSchema=mongoose.Schema({
    parentChannel:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Channel'
    },
    day:String,
    date:String,
    start_time:String,
    end_time:String,
    // program:{
    //     type:mongoose.Schema.Types.ObjectId,
    //     ref:'Program'
    // },
    program:String,
    episode:String,
    shortDescription:String,
    rj:String
    
    
});
 
var Schedule=module.exports=mongoose.model('Schedule', scheduleSchema);
