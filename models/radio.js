var mongoose=require('mongoose');


//Radio_Schema
var radioSchema=mongoose.Schema({
  pradeshName:{
      type:String,
      required:true
},  
radios:[
    {
        radioName:String,
        radioImage:String,
        radioValue:String
    }
]
});
 
var Radio=module.exports=mongoose.model('Radio', radioSchema);
