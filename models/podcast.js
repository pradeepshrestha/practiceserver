var mongoose=require('mongoose');

var podcastSchema=mongoose.Schema({
    programName:{type:String,required:true},  
    description:String,
    parentChannelID:String,
    episodes:[{
        episode:String,
        title:String,
        //rj:String,
     url:String
    
        // startTime:String,
        // endTime:String,
        // size:String,
        // uploadDate:String
    }]
   
    
    // parentOrganization:{
    //     type:mongoose.Schema.Types.ObjectId,
    //     ref:'Radioorganization'
    // }
});
 
var Podcast=module.exports=mongoose.model('Podcast', podcastSchema);
