var mongoose=require('mongoose');

var rjSchema=mongoose.Schema({
    rjName:{type:String,required:true},  
    rjImage:String,
    details:String,
    parentPrograms:[
        {
            type:mongoose.Schema.Types.ObjectId,
            ref:'Channel'
        }
    ],
    
   // schedule:[],//{type:mongoose.Schema.Types.ObjectId,ref:'Schedule'}
    
    parentChannel:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Channel'
    }
});
 
var RJProfile=module.exports=mongoose.model('RjProfile', rjSchema);
