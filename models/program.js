var mongoose=require('mongoose');

var programSchema=mongoose.Schema({
    programName:{type:String,required:true},  
    programLogo:String,
    rjProfiles:[
        // {
        // type:mongoose.Schema.Types.ObjectId,
        // ref:'RjProfile'
        // }
    ],
    
   // schedule:[],//{type:mongoose.Schema.Types.ObjectId,ref:'Schedule'}
    
    parentChannel:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Channel'
    },
    //start:optional -used for schedule
    episode:String,
    shortDescription:String,
    //end:optional -used for schedule
    episodes:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Episode'
    }]

});
 
var Program=module.exports=mongoose.model('Program', programSchema);
