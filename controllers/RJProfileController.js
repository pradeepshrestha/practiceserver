var RJProfile=require('../models/rjprofile');
//var Program=require('../models/program');

//get all episodes
module.exports.getAllRJProfiles=function(req,res,next){
    RJProfile.find()
    .exec()
    .then(rjprofiles=>{
        res.status(500).json({rjprofiles});
    })  
    .catch(err=>{
        res.status(500).json({err});    
    });
  }


  //get episode by id
  module.exports.getRJProfileById=function(req,res,next){
    const id=req.params.id;
    RJProfile.findById(id)
    .exec()
    .then(rjprofile=>{
        res.status(200).json(rjprofile);
    })
    .catch(err=>{
        res.status(500).json({err});
    });
 }



//create radioChannel
module.exports.createRJProfile=function(req,res,next){
    
    const channelId=req.params.channelId;
    const newRJProfile=new RJProfile(req.body);
    newRJProfile.parentChannel=channelId;
    newRJProfile.save()
    .then(rjprofile=>        
        {
            //add episode objectid to its parent Program array named episodes
    //    Program.update({_id:programId}, 
    //     {$push: {episodes: episode._id}})
    //     .exec()
    //     .then(numberAffected=>{console.log(numberAffected);})
    //     .catch(err=>
    //         {
    //             res.status(500).json({err});
    //         }
    //     ); 
            
        res.status(200).json(rjprofile);
        }
    )
    .catch(err=>{
        res.status(500).json({err:"saving error"});
    });
}
        
      

//update episode by id
module.exports.updateRJProfile=function(req,res,next){
 const id=req.params.id;
 RJProfile.findByIdAndUpdate(id,req.body)
 .exec()
 .then(updatedRJProfile=>{
    res.status(200).json(updatedRJProfile);
 })
 .catch(err=>{
    res.status(500).json({err});
 });   
 }



//delete episode by id
module.exports.deleteRJProfile=function(req,res,next){
    const id=req.params.id;

    RJProfile.findByIdAndRemove(id).
    exec()
    .then(rjprofile=>{
        res.status(200).json(rjprofile);
    }).catch(err=>{

    });
}
    
         



 
   //get post by id
   module.exports.getRJProfileOfChannel=function(req,res,next){
    const channelId=req.params.channelId;
    RJProfile.find({parentChannel:channelId},(err,rjprofiles)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(rjprofiles);
    });
 }