var Channel=require('../models/channel');
var RadioOrganization=require('../models/radio_org');
var Program=require('../models/program');
var path = require('path');
//var q=require('q');

//create radioChannel
module.exports.createChannel=function(req,res,next){
  const host = req.hostname;
  
   const filePath = "/" + host + ':3000/' + req.file.path;
  // console.log(filePath);
   var correctedPath = path.normalize(filePath); //that's that
//console.log("pop:"+correctedPath);
   lastPath=req.protocol + ":\\"+correctedPath;
console.log(lastPath);
//console.log(req.file.path);
  const id=req.params.id;
  const newChannel=new Channel(req.body);
newChannel.parentOrganization=id;
newChannel.channelLogo=lastPath;
newChannel.save(
    (err,channel)=>{
    if(err){
      res.status(500).json({err});
    }
     console.log(channel._id);
     //add channel objectid to its parent Organization array named radioChannels
     RadioOrganization.update({_id:id}, 
      {$push: {radioChannels: channel._id}}, 
          function (err, numberAffected) {
            console.log(numberAffected);
      });
            res.status(200).json(channel);
     });  
    }

//get all posts
module.exports.getAllChannels=function(req,res,next){
    Channel.find((err,channels)=>{
     if(err){
      res.status(500).json({err});
    }
     res.status(200).json(channels);
    });  
  }
  //get all channels by region/pradesh
module.exports.getAllChannelsByPradesh=function(req,res,next){
  const regionName=req.params.regionName;
  
  Channel.find(
    {region:regionName},
  (err,channels)=>{
   if(err){
    res.status(500).json({err});
  }
   res.status(200).json(channels);
  });  
}



  //get post by id
  module.exports.getChannelById=function(req,res,next){
    const id=req.params.id;
    Channel.findById(id,(err,channel)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(channel);
    });
 }


 //get channels by parent organizationId
 module.exports.getChannelByOrganizationId=function(req,res,next){
  const id=req.params.organizationId;
  Channel.find({parentOrganization:id})
  .exec()
  .then(channelsByOrganizationID=>{
    res.status(200).json({channelsByOrganizationID});
  })
  .catch(err=>{
    res.status(500).json({err});
  });
    
    
}




//update post by id
module.exports.updateChannel=function(req,res,next){
 const id=req.params.id;
 Channel.findByIdAndUpdate(id,req.body, (err,channel)=>{
  if(err){
    res.status(500).json({err});
  }
   res.status(200).json(Channel);
 });   
 }



//delete channel by id
module.exports.deleteChannel=function(req,res,next){
    const id=req.params.id;

    Channel.findByIdAndRemove(id,(err,channel)=>{
     
    if(!err) {
      RadioOrganization.update({_id: channel.parentOrganization}, 
           {$pull: {radioChannels: channel._id}}, 
               function (err, numberAffected) {
                 console.log(numberAffected);
           });
           
      // res.status(200).json(channel);

 
           //if parent channel is deleted,delete all its children programs from program collection with help of "parentChannel" field of program table
  //-------------------------------------------------------start
  Program.remove({ parentChannel: channel._id }, function(err,program) {
    // if (!err) {
    //         // message.type = 'notification!';
    //         res.status(200).json(program);
    // }
    // else {
    //         message.type = 'error';
    // }
});
 
  //=======================================================end
 
  res.status(200).json(channel);
          } //closing if(!err)
           else {
             console.log(err);                                      
         }
        
      });
 }

//correct alternative way i did to create channel with use of parent organozation id and add object id of that created channel to the radiochannels array of parentOrg.
//source:https://stackoverflow.com/questions/32674280/removing-one-one-and-one-many-references-mongoose
//  //create radioChannel
// module.exports.createChannel=function(req,res,next){
//   const id=req.params.id;
//   const newChannel=new Channel(req.body);
// newChannel.parentOrganization=id;


//   newChannel.save(
//     (err,channel)=>{
//     if(err){
//       res.status(500).json({err});
//     }
//      res.status(201).json(channel);
//      console.log(channel._id);
     

//     //-----------------------
//     RadioOrganization.findById(id,function(err,org){
      
// org.radioChannels.push(channel._id);
// console.log(org);
// const newOrg=new RadioOrganization(org);
// newOrg.save((err,organization)=>{
// console.log("yoman");
// });//closing newOrg.save function
//     })
 

//   });
 
//  }