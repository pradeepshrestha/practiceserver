var Program=require('../models/program');
// var Channel=require('../models/channel');
//get all posts
module.exports.getAllPrograms=function(req,res,next){
  console.log("yoman");
    Program.find((err,programs)=>{
     if(err){
      res.status(500).json({err});
    }
     res.status(200).json(programs);
    });  
  }


  //get post by id
  module.exports.getProgramById=function(req,res,next){
    const id=req.params.id;
    Program.findById(id,(err,program)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(program);
    });
 }
  //get programs by parent channel id
 
 module.exports.getProgramsByChannelId=function(req,res,next){
  const id=req.params.channelId;
  Program.find({parentChannel:id})
  .exec()
  .then(programsByChannelID=>{
    res.status(200).json({programsByChannelID});
  })
  .catch(err=>{
    res.status(500).json({errpppppp});
  });
    
    
}



//create radioChannel
module.exports.createProgram=function(req,res,next){
    
    const channelId=req.params.channelId;
    const newProgram=new Program(req.body);
  newProgram.parentChannel=channelId;
  newProgram.save(
      (err,program)=>{
      if(err){
        res.status(500).json({err});
      }
       
      
       //add program objectid to its parent Channel array named programs
    //    RadioOrganization.update({_id:id}, 
    //     {$push: {radioChannels: channel._id}}, 
    //         function (err, numberAffected) {
    //           console.log(numberAffected);
    //     });
        
        res.status(200).json(program);
  
       });  
      }

//update program by id
module.exports.updateProgram=function(req,res,next){
 const id=req.params.id;
 Program.findByIdAndUpdate(id,req.body, (err,program)=>{
  if(err){
    res.status(500).json({err});
  }
   res.status(200).json(program);
 });   
 }


//delete program by id
module.exports.deleteProgram=function(req,res,next){
    const id=req.params.id;

    Program.findByIdAndRemove(id,(err,program)=>{
    if(err){
      res.status(500).json({err});
    }

    //if parent channel is deleted,delete all its children programs with help of objId that have been store in array named programs in parentChannel schema/model
    //-------------------------------------------------------
    // radioOrganization.radioChannels.forEach(function(element) {
    //   Channel.findByIdAndRemove(element,(err,channel)=>{
    //     // if(err){
    //     //   res.status(500).json({err});
    //     // }
        
        
                 
    //       });  //close Channel.findByIdAndRemove 
    // }, this); //close forEach
   
    //=======================================================
     res.status(200).json(program);
    }); //close Program.findByIdAndRemove 
 }//close function deleteProgram



 
   //get post by id
   module.exports.getProgramsOfChannel=function(req,res,next){
    const channelId=req.params.channelId;
    Program.find({parentChannel:channelId},(err,programs)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(programs);
    });
 }