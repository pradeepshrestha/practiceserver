var Radio=require('../models/radio');

//create and update services++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
module.exports.editRadio=function(req,res,next){
  const pradeshName=req.params.pradeshName;
  const linkid=req.params.linkid;
  
  Radio.findOneAndUpdate(
         //Radio.find()[0].radios.
      {
       pradeshName:pradeshName,
        'radios._id':linkid
      
      },
      {$set:{'radios.$':req.body }},
      //  {$push:{radios:radio}},
       
       {upsert:true},
       function(err, radio){
       if(err){
         res.json({success:false, err:err});
       }
       if(radio){
         res.json({success:true,radio:radio});
       }
       else {
         res.json({success:false , msg:"Something went wrong "});
       }

     });
     

 }


 
 //get all radios
 module.exports.getAllRadios=function(req,res,next){
      Radio.find((err,radios)=>{
      if(err){
       res.status(500).json({err});
     }
      res.status(200).json(radios);
     });  
   }
 
 

 
 //create radio
 module.exports.createRadio=function(req,res,next){
     const pradeshName=req.body.pradeshName;
     const radioName=req.body.radioName;
     const radioImage=req.body.radioImage;
     const radioValue=req.body.radioValue;
 
     const radio=new Radio({
        pradeshName:pradeshName,
       radios:[ 
           {radioName:radioName,radioImage:radioImage,radioValue:radioValue}
       ]
     });
  radio.save((err,radio)=>{
  
 if(err){
     res.status(500).json({err});
   }
    res.status(201).json(radio);
 });
  }
 

//create and update services++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
module.exports.updateRadio=function(req,res,next){
  const pradeshName=req.params.pradeshName;
  const radioName=req.body.radioName;
  const radioValue=req.body.radioValue;
  const radioImage=req.body.radioImage;
  

     var radio={     
 "radioName":radioName,
  "radioValue":radioValue,
  "radioImage":radioImage
     };

     Radio.findOneAndUpdate(
      // Radio.find()[0].radios.findOneAndUpdate(
      {
       pradeshName:pradeshName
      },
      
       {$push:{radios:radio}},
       
       {
       upsert:true,
        'new':true,
     },function(err, radio){
       if(err){
         res.json({success:false, err:err});
       }
       if(radio){
         res.json({success:true,radio:radio});
       }
       else {
         res.json({success:false , msg:"Something went wrong "});
       }

     });
     

 }


// // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//update post by id
// module.exports.editRadio=function(req,res,next){
  // const linkid=req.params.linkid;
  //        Radio.find()[0].radios.findByIdAndUpdate({_id:linkid},req.body, (err,radio)=>{
  //  if(err){
  //    res.status(500).json({err});
  //  }
  //   res.status(200).json({radio});
  // });   
  // }


//create and update services++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
module.exports.deleteRadio=function(req,res,next){
  const pradeshName=req.params.pradeshName;
   const linkid=req.params.linkid;
  
     Radio.findOneAndUpdate(
      
      {
       pradeshName:pradeshName
      },
       
       {$pull:{radios:{_id:linkid}}},
       function(err,doc){
        if(!err)
          res.json({"msg":"deleted successfully"});
           // console.log("Online list modified: ",doc);
        else
            console.log("Online list modified error :",err);
    }

  

     );
     

 }

//  //update post by id
//  module.exports.updateSlider=function(req,res,next){
//   const id=req.params.id;
//   Slider.findByIdAndUpdate(id,req.body, (err,slider)=>{
//    if(err){
//      res.status(500).json({err});
//    }
//     res.status(200).json({slider});
//   });   
//   }
 
 
//  //delete post by id
//  module.exports.deleteSlider=function(req,res,next){
//      const id=req.params.id;
 
//      Slider.findByIdAndRemove(id,(err,slider)=>{
//      if(err){
//        res.status(500).json({err});
//      }
//       res.status(200).json({slider});
//      });
//   }
