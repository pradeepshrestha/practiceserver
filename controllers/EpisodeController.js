var Episode=require('../models/episode');
var Program=require('../models/program');

//get all episodes
module.exports.getAllEpisodes=function(req,res,next){
    Episode.find()
    .exec()
    .then(episodes=>{
        res.status(500).json({episodes});
    })  
    .catch(err=>{
        res.status(500).json({err});    
    });
  }


  //get episode by id
  module.exports.getEpisodeById=function(req,res,next){
    const id=req.params.id;
    Episode.findById(id)
    .exec()
    .then(episode=>{
        res.status(200).json(episode);
    })
    .catch(err=>{
        res.status(500).json({err});
    });
 }



//create radioChannel
module.exports.createEpisode=function(req,res,next){
    
    const programId=req.params.programId;
    const newEpisode=new Episode(req.body);
    newEpisode.parentProgram=programId;
    newEpisode.save()
    .then(episode=>        
        {
            //add episode objectid to its parent Program array named episodes
       Program.update({_id:programId}, 
        {$push: {episodes: episode._id}})
        .exec()
        .then(numberAffected=>{console.log(numberAffected);})
        .catch(err=>
            {
                res.status(500).json({err});
            }
        ); 
            
        res.status(200).json(episode);
        }
    )
    .catch(err=>{
        res.status(500).json({err:"saving error"});
    });
}
        
      

//update episode by id
module.exports.updateEpisode=function(req,res,next){
 const id=req.params.id;
 Episode.findByIdAndUpdate(id,req.body)
 .exec()
 .then(updatedEpisode=>{
    res.status(200).json(updatedEpisode);
 })
 .catch(err=>{
    res.status(500).json({err});
 });   
 }



//delete episode by id
module.exports.deleteEpisode=function(req,res,next){
    const id=req.params.id;

    Episode.findByIdAndRemove(id).
    exec()
    .then(episode=>{
        res.status(200).json(episode);
    }).catch(err=>{

    });
}
    
         



 
   //get post by id
   module.exports.getProgramsOfChannel=function(req,res,next){
    const channelId=req.params.channelId;
    Program.find({parentChannel:channelId},(err,programs)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(programs);
    });
 }