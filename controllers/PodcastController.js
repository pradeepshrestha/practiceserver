var Podcast=require('../models/podcast');
var path=require('path');


//get all podcast programs
module.exports.getAllPodcasts=function(req,res,next){
    
    Podcast.find()
    .exec()
    .then(podcasts=>{
        res.status(200).json({podcasts});
        
    })  
    .catch(err=>{
        res.status(500).json({err});    
    });
  }


  //get episode by id
  module.exports.getPodcastById=function(req,res,next){
    const id=req.params.id;
    Podcast.findById(id)
    .exec()
    .then(podcast=>{
        res.status(200).json(podcast);
    })
    .catch(err=>{
        res.status(500).json({err});
    });
 }



//create radioChannel
module.exports.createPodcast=function(req,res,next){
    // const host = req.hostname;
  
    // const filePath = "/" + host + ':3000/' + req.file.path;
    //   var correctedPath = path.normalize(filePath);
    // lastPath=req.protocol + ":\\"+correctedPath;
      
    const newPodcast=new Podcast(req.body);
    // newPodcast.url=lastPath;  
    //console.log(newPodcast);  

    // newEpisode.parentProgram=programId;
    newPodcast.save()
    .then(podcast=>        
        {
        res.status(200).json(podcast);
        }
    )
    .catch(err=>{
        res.status(500).json({err:"saving error"});
    });
}
        
      

//update episode by id
module.exports.updatePodcast=function(req,res,next){
 const id=req.params.id;
 Podcast.findByIdAndUpdate(id,req.body)
 .exec()
 .then(updatedPodcast=>{
    res.status(200).json(updatedPodcast);
 })
 .catch(err=>{
    res.status(500).json({err});
 });   
 }



//delete episode by id
module.exports.deletePodcast=function(req,res,next){
    const id=req.params.id;

    Podcast.findByIdAndRemove(id).
    exec()
    .then(podcast=>{
        res.status(200).json(podcast);
    }).catch(err=>{

    });
}
    
         



 
   //get post by id
   module.exports.getPodcastsOfChannel=function(req,res,next){
    const channelId=req.params.parentChannelID;
    Podcast.find({parentChannelID:channelId},(err,podcasts)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json({podcasts});
    });
 }


// ==========podcastEpisodes=========podcastEpisodes=====podcastEpisodes===podcastEpisodes===podcastEpisodes
module.exports.updateEpisodesOfPodcast=function(req,res,next){
    console.log(req.file);
    // 1111111111111111111111111111111111111111111111111111111111111111111111


    const host = req.hostname;
  
   const filePath = "/" + host + ':3000/' + req.file.path;
  // console.log(filePath);
   var correctedPath = path.normalize(filePath); //that's that
//console.log("pop:"+correctedPath);
   lastPath=req.protocol + ":\\"+correctedPath;
console.log(lastPath);
//console.log(req.file.path);
  


    //end----1111111111111111111111111111111111111111111111111111111111111111111111


    // const prgramName=req.params.programName;
    const programID=req.params.id;
    //console.log(programID);
    var episode={     
        "episode":req.body.episode,
        "title":req.body.title,
    //    "rj":req.body.rj
    "url":lastPath
       };
      
  
       Podcast.findOneAndUpdate(
        // Radio.find()[0].radios.findOneAndUpdate(
        {
        //  programName:prgramName
        '_id':programID
        },
        
        {$push:{episodes:episode}},
         
         {
         upsert:true,
          'new':true,
       },function(err, episode){
         if(err){
           res.json({success:false, err:err});
         }
         if(episode){
           res.json({success:true,episode:episode});
         }
         else {
           res.json({success:false , msg:"Something went wrong "});
         }
  
       });
       
  
   }

