var Client=require('../models/client');
const bcrypt = require('bcryptjs');

//get all episodes
module.exports.getAllClients=function(req,res,next){
    Client.find()
    .exec()
    .then(clients=>{
        res.status(500).json({clients});
    })  
    .catch(err=>{
        res.status(500).json({err});    
    });
  }


  //get episode by id
  module.exports.getEpisodeById=function(req,res,next){
    const id=req.params.id;
    Episode.findById(id)
    .exec()
    .then(episode=>{
        res.status(200).json(episode);
    })
    .catch(err=>{
        res.status(500).json({err});
    });
 }



//create radioChannel
module.exports.createClient=function(req,res,next){
    
    // const username=req.body.username;
    // const email=req.body.username;
    // const password=req.body.username;
    const newClient=new Client(req.body);
    
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newClient.password, salt, (err, hash) => {
        if(err) throw err;
        newClient.password = hash;
       
        newClient.save();
      });
    });
    
}
        
      

//update episode by id
module.exports.updateEpisode=function(req,res,next){
 const id=req.params.id;
 Episode.findByIdAndUpdate(id,req.body)
 .exec()
 .then(updatedEpisode=>{
    res.status(200).json(updatedEpisode);
 })
 .catch(err=>{
    res.status(500).json({err});
 });   
 }



//delete episode by id
module.exports.deleteEpisode=function(req,res,next){
    const id=req.params.id;

    Episode.findByIdAndRemove(id).
    exec()
    .then(episode=>{
        res.status(200).json(episode);
    }).catch(err=>{

    });
}
    
         



 
   //get post by id
   module.exports.getProgramsOfChannel=function(req,res,next){
    const channelId=req.params.channelId;
    Program.find({parentChannel:channelId},(err,programs)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(programs);
    });
 }