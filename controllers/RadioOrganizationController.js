var RadioOrganization=require('../models/radio_org');
var Channel=require('../models/channel');

// const multer = require('multer');
// const upload=multer({dest:'uploads/'});



//get all posts
module.exports.getAllRadioOrganizations=function(req,res,next){
     RadioOrganization.find((err,radioOrganizations)=>{
     if(err){
      res.status(500).json({err});
    }
     res.status(200).json(radioOrganizations);
    });  
  }


  //get post by id
  module.exports.getRadioOrganizationById=function(req,res,next){
    const id=req.params.id;
    RadioOrganization.findById(id,(err,radioOrganization)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(radioOrganization);
    });
 }

// ----------------------------------
// organizationName:{type:String,required:true},  
// orgLogo:{type:String},
// radioChannels:[{type:mongoose


module.exports.organizationImage=function(req,res,next){
  console.log(req.file);
  res.send("yoman!!!");
}

//create post
module.exports.createRadioOrganization=function(req,res,next){
    // const organizationName=req.body.organizationName;
    // const orgLogo=req.body.orgLogo;
    // const radioChannels=[1,2,3];
console.log(req.file);
    
const radioOrganization=new RadioOrganization({

   organizationName:req.body.organizationName,
   orgLogo:req.body.orgLogo
  
});
radioOrganization.radioOrganizationImage=req.file.path; 
radioOrganization.save((err,radioOrganization)=>{
  if(err){
    res.status(500).json({err});
  }
   res.status(201).json(radioOrganization);
});
 }

//update post by id
module.exports.updateRadioOrganization=function(req,res,next){
 const id=req.params.id;
 RadioOrganization.findByIdAndUpdate(id,req.body, (err,radioOrganization)=>{
  if(err){
    res.status(500).json({err});
  }
   res.status(200).json(radioOrganization);
 });   
 }


//delete post by id
module.exports.deleteRadioOrganization=function(req,res,next){
    const id=req.params.id;

    RadioOrganization.findByIdAndRemove(id,(err,radioOrganization)=>{
    if(err){
      res.status(500).json({err});
    }

    //if parent radioOrg is deleted,delete all its children radioChannels with help of objId that have been store in array named radioChannels in radioOrganization cshema/model
    //-------------------------------------------------------
    radioOrganization.radioChannels.forEach(function(element) {
      Channel.findByIdAndRemove(element,(err,channel)=>{
        // if(err){
        //   res.status(500).json({err});
        // }
        
        
                 
          });  //close Channel.findByIdAndRemove 
    }, this); //close forEach
   
    //=======================================================
     res.status(200).json(radioOrganization);
    }); //close RadioOrganization.findByIdAndRemove 
 }//close function deleteRadioOrganization



 
   //get post by id
   module.exports.getChannelsOfRadioOrganization=function(req,res,next){
    const radioOrganizationId=req.params.radioOrganizationId;
    Channel.find({parentOrganization:radioOrganizationId},(err,channels)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(channels);
    });
 }