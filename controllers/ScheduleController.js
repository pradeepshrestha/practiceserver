var Schedule=require('../models/schedule');
// var Program=require('../models/program');

//get all episodes
module.exports.getAllSchedules=function(req,res,next){
    Schedule.find()
    .exec()
    .then(schedules=>{
        res.status(500).json({schedules});
    })  
    .catch(err=>{
        res.status(500).json({err});    
    });
  }


  //get episode by id
  module.exports.getScheduleById=function(req,res,next){
    const id=req.params.id;
    
    Schedule.findById(id)
    .exec()
    .then(schedule=>{
        res.status(200).json(schedule);
    })
    .catch(err=>{
        res.status(500).json({err});
    });
 }
 //get by day
  //get schedules by id
  module.exports.getSchedulesByDay=function(req,res,next){
    const channelId=req.params.channelId;
    const dayName=req.params.dayName;
    Schedule.find({
        day:dayName,
        parentChannel:channelId
    })
    .exec()
    .then(schedules=>{
        res.status(200).json(schedules);
    })
    .catch(err=>{
        res.status(500).json({err});
    });
 }



//create radioChannel
module.exports.createSchedule=function(req,res,next){
    
    const channelId=req.params.channelId;
    const newSchedule=new Schedule(req.body);
    newSchedule.parentChannel=channelId;
    newSchedule.save()
    .then(schedule=>        
        {
            res.status(200).json(schedule);
        }
    )
    .catch(err=>{
        res.status(500).json({err:"saving error"});
    });
}
        
      

//update episode by id
module.exports.updateSchedule=function(req,res,next){
 const id=req.params.id;
 Schedule.findByIdAndUpdate(id,req.body)
 .exec()
 .then(updatedSchedule=>{
    res.status(200).json(updatedSchedule);
 })
 .catch(err=>{
    res.status(500).json({err});
 });   
 }



//delete episode by id
module.exports.deleteSchedule=function(req,res,next){
    const id=req.params.id;

    Schedule.findByIdAndRemove(id).
    exec()
    .then(schedule=>{
        res.status(200).json(schedule);
    }).catch(err=>{

    });
}
    
         



 
   //get post by id
   module.exports.getScheduleOfChannel=function(req,res,next){
    const channelId=req.params.channelId;
    Schedule.find({parentChannel:channelId},(err,programs)=>{
      if(err){
        res.status(500).json({err});
      }
       res.status(200).json(programs);
    });
 }