const passport = require('passport');
var User=require('../models/user');
const jwt = require('jsonwebtoken');
require('../config/passport')(passport);

const config = require('../config/database');

//get all posts
module.exports.getAllEmployees=function(req,res,next){
     User.find((err,employees)=>{
     if(err){
      res.status(500).json({err});
    }
     res.status(200).json({employees});
    });  
  }


//====================================================================

module.exports.registerUser=function(req, res, next){

    
    let newUser = new User({
      name: req.body.name,
      email: req.body.email,
      username: req.body.username,
      password: req.body.password
    });
  
    User.addUser(newUser, (err, user) => {
      if(err){
        res.json({success: false, msg:'Failed to register user'});
      } else {
        res.json({success: true, msg:'User registered'});
      }
    });
  }
//========================================================================================
  module.exports.authenticateUser=function(req, res, next) {
    const username = req.body.username;
    const password = req.body.password;
  
    User.getUserByUsername(username, (err, user) => {
      if(err) throw err;
      if(!user){
        return res.json({success: false, msg: 'User not found'});
      }
  
      User.comparePassword(password, user.password, (err, isMatch) => {
        if(err) throw err;
        if(isMatch){
          const token = jwt.sign(user, config.secret, {
            expiresIn: 604800 // 1 week
          });
          
             res.json({
            success: true,
            token:'JWT '+token,
             user: {
              id: user._id,
              name: user.name,
              username: user.username,
              email: user.email
            }
          });
        } else {
          return res.json({success: false, msg: 'Wrong password'});
        }
      });
    });
  }
  //===================================================================================

  module.exports.getProfile=function(req, res, next){ 
    
    res.json({user: req.user});
    
  }